import {CharStreams, CommonTokenStream } from "antlr4ts";
import {SnowflakeLexer} from "../grammar/SnowflakeLexer"
import {SnowflakeParser} from "../grammar/SnowflakeParser"
import { CodeCompletionCore } from "antlr4-c3";
import { Scanner } from "ts-antlr4-scanner";
import {Completion} from "@codemirror/autocomplete";
import { EditorView } from "@codemirror/view"

export default function queryParser(query: string, caretPosition: number) {
  console.clear()
  const inputStream = CharStreams.fromString(query.trimEnd().toUpperCase())
  const lexer = new SnowflakeLexer(inputStream)
  const tokenStream = new CommonTokenStream(lexer)
  const parser = new SnowflakeParser(tokenStream)
  // Do not raise errors when the parser fails to completely process the query. As our responsibility
  // is autocomplete, the majority of the time parsing will fail at some point.
  parser.removeErrorListeners()

  const context = parser.snowflake_file()

  const scanner = new Scanner(tokenStream)
  scanner.advanceToPosition(caretPosition)
  scanner.push()

  let caretIndex = scanner.tokenIndex()

  // If the previous character is NOT a space (ie: we're actively typing a word),
  // decrement the suggestion to the previous index
  if(query.charAt(caretPosition - 1) !== " ") {
    --caretIndex
  }

  const core = new CodeCompletionCore(parser)
  core.preferredRules = new Set([
    SnowflakeParser.RULE_object_name,
    SnowflakeParser.RULE_id_,
    SnowflakeParser.RULE_string
  ])

  console.debug("Caret Index:", caretIndex)
  const candidates = core.collectCandidates(caretIndex, context)

  const keywords = []
  for (let candidate of candidates.tokens) {
    const completions = []
    const [token, followupTokens] = candidate
    completions.push(unquote(parser.vocabulary.getDisplayName(token)))
    if (followupTokens.length) {
      for (let token of followupTokens) {
        completions.push(unquote(parser.vocabulary.getDisplayName(token)))
      }
    }
    keywords.push({ label: completions.join(" ") })
  }

  const objects: Completion[] = []
  for (let candidate of candidates.rules) {
    switch (candidate[0]) {
      case SnowflakeParser.RULE_object_name:
        objects.push({ label: "mytablename", type: "table" } )
        objects.push({ label: "othertablename", type: "table" } )
        objects.push({ label: "pretendtablename", type: "table" } )
        break;
      case SnowflakeParser.RULE_id_:
        objects.push({ label: "name" })
        break;
      case SnowflakeParser.RULE_string:
        objects.push({ label: "'string'" })
    }
  }
  console.debug("Keywords:", keywords)
  console.debug("Objects:", objects)

  return [
    ...objects,
    ...keywords,
  ]
}

export function unquote(text?: string): string {
  if (!text) return ''
  if (text.length < 2) return text
  if (
    text.startsWith('"') ||
    text.startsWith('`') ||
    (text.startsWith("'") && text.startsWith(text[text.length - 1]))
  ) {
    return text.slice(1, text.length - 1)
  }
  return text
}
