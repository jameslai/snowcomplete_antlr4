import CodeMirror from "@uiw/react-codemirror"
import { ViewUpdate } from "@codemirror/view"
import { autocompletion, Completion } from "@codemirror/autocomplete"
import {useCallback, useEffect, useState} from "react";
import queryParser from "./lib/queryparser";

interface QueryInfo {
  query: string;
  caretPosition: number;
}

export default function AutoComplete() {

  const [queryInfo, setQueryInfo] = useState<QueryInfo>({ query: '', caretPosition: 0 })
  const [completions, setCompletions] = useState<Completion[]>([])

  useEffect(() => {
    if(queryInfo.query.length > 0) {
      const result = queryParser(queryInfo.query, queryInfo.caretPosition)
      setCompletions(result)
    } else {
      clearCompletions()
    }
  }, [queryInfo])

  const clearCompletions = () => {
    setCompletions([])
  }

  const onChange = useCallback((value: string, viewUpdate: ViewUpdate) => {
    const selection = viewUpdate.state.selection.asSingle()
    setQueryInfo({
      query: value,
      caretPosition: selection.ranges[0].from
    })
  }, [])

  return (
    <CodeMirror
      extensions={[
        autocompletion({
          override: [(context) => {
            let word = context.matchBefore(/\w*/)
            // Uncomment if it is not desirable to show autocomplete before the user
            // has typed anything
            // if (word?.from === word?.to && !context.explicit) return null
            return {
              from: word?.from || 0,
              options: completions
            }
          }]
        })
      ]}
      onChange={onChange}
    />
  )
}