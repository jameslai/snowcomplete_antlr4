import AutoComplete from "./AutoComplete";

function App() {
  return (
    <div>
      <h1>Snowflake Smart AutoComplete Demo</h1>
      <AutoComplete />
      <p>This demo showcases Snowflake with smart auto complete. The algorithm will only make syntactically correct suggestions.</p>
      <p>A couple features to try:</p>
      <ol>
        <li>The algorithm is aggressive - press your spacebar to move to the next token and receive a suggestion without needing to type the next character</li>
        <li>When the only valid option is multi-token (for example, "IF NOT EXISTS", "API INTEGRATION", "ROW ACCESS POLICY", "RESOURCE MONITOR", etc), the algorithm will suggest the entire token set</li>
        <li>We cannot get the list of object names (since we are not authenticated) but this demo provides an example of where that hook is possible. When entering "CREATE TABLE ...", a list of manually inserted objects displays as lowercase choices. In theory, this would be very simple to replace with a list of actual object names.</li>
        <li>Statements like "CREATE API INTEGRATION" follow a very strict set of options - try it out! The editor practically forces you into the pit of success</li>
        <li>Don't forget to try auto complete on things like column names during table creation - very handy!</li>
      </ol>
      <p>How was this built?</p>
      <ol>
        <li>We leverage the <a href="https://github.com/antlr/grammars-v4/blob/master/sql/snowflake/SnowflakeParser.g4">ANTLR4 Snowflake grammars provided in the official ANTLR repository</a></li>
        <li>The Snowflake lexer and parser grammars are compiled and provided in-browser at runtime</li>
        <li>CodeMirror triggers a custom auto complete function made for this demo</li>
        <li>Leveraging the Snowflake lexer and parser, the query is processed, lexical position calculated and the next valid token(s) are collected</li>
        <li>Custom rules are added to replace certain tokens (such as "id_", "object_name" and "STRING") with custom responses</li>
      </ol>
      <p>How is this different from Snowsight's current auto complete algorithm?</p>
      <ol>
        <li>The list of keywords and functions were scraped from Snowflake's documentation site in 2020 and they are provided as a flat list</li>
        <li>Any of those keywords or function names can be suggested at any time, resulting in the editor frequently suggesting invalid syntax</li>
      </ol>
      <p>Known issues</p>
      <ol>
        <li>Being that the SQL editor is often composed of multiple statements, we support autocomplete over multiple statements. At the moment once finishing a statement, the auto complete will suggest continuing on immediately with a new one.</li>
        <li>Once you have a syntactical error, auto complete stops as it cannot finish parsing. It is possible to have a syntactical error further up the document resulting a suspension of suggestions</li>
        <li>Within the grammar, all object names are indistinguishable from each other as they all use the term "object_name". To identify what object(s) we should display, we would (likely) need to update the grammar to provide hints what we should display at this position (table, column, schema, integration, user, etc)</li>
      </ol>
      <p>Where to go next</p>
      <ol>
        <li>Offload auto complete calculation to a WebWorker to prevent any computationally expensive calculations from blocking the main thread</li>
        <li>Populate object names</li>
        <li>Generate syntax highlighting rules</li>
        <li>Generate linting rules</li>
        <li>Add rules/update grammar to prevent unexpected suggestions</li>
        <li>Add all <a href="https://docs.snowflake.com/en/sql-reference-functions">Snowflake SQL functions</a> to grammar</li>
        <li>Add descriptions to suggestions (usually the first line from the documentation)</li>
      </ol>
    </div>
  );
}

export default App;
